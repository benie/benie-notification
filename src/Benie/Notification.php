<?php
namespace Benie;

class Notification {
	public static function sendEmail($options = array()) {
		$options = array_merge(array(
			'recipients' => 'notifications@benie.com.au',
			'sender' => Notification::getVar('BenieMailFrom', 'MailFrom', 'noreply@benie.com.au'),
			'subject' => 'No subject',
			'body' => 'No body',
			'plain_text' => 'No plain text',
		), $options);

		$email = new \Benie\Notification\Email;

		$email->setSender($options['sender']);
		$email->setRecipients($options['recipients']);
		$email->setSubject($options['subject']);
		$email->setBody($options['body']);
		$email->setPlainText($options['plain_text']);
		$result = $email->send();

		return $result;
	}

	public static function sendSMS($options = array()) {
		$options = array_merge(array(
			'recipients' => '',
			'sender' => Notification::getVar('BenieSMSFrom', 'SMSFrom', 'BenieLive'),
			'body' => '',
		), $options);

		$sms = new \Benie\Notification\SMS;
		$sms->setSender($options['sender']);
		$sms->setRecipients($options['recipients']);
		$sms->setBody($options['body']);
		$result = $sms->send();

		return $result;
	}

	/**
	 * Retrieve an environment variable, or fall back to default
	 * @usage getVar('x', 'y', 'z', ..., 'default')
	 */
	public static function getVar() {
		$args = func_get_args();
		$default = array_pop($args);
		$result = false;

		// Loop through all of the environment variables provided until one exists
		foreach($args as $arg)
			if($result = getenv($arg)) break;

		// If nothing exists, use the default
		if(empty($result)) $result = $default;

		return $result;
	}
}