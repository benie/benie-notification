<?php
namespace Benie\Notification;

class Email extends BaseNotification {
	use \Benie\Notification\Email\SES;

	// Extra variables specific to emails
	var $subject = '';
	var $headers = array();

	/**
	 * Make sure the recipient is a valid address, or array of addresses.
	 */
	public function setRecipients($recipients) {
		if(empty($recipients)) return false;
		if(!is_array($recipients)) $recipients = array($recipients);

		foreach($recipients as $recipient) {
			if(filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
				$this->addRecipient($recipient);
			}
		}

		return false;
	}

	/**
	 * [setPlainText description]
	 * @param string $plain_text [description]
	 */
	public function setPlainText($plain_text = '') {
		$this->plain_text = $plain_text;
	}

	/**
	 * [setSubject description]
	 * @param string $subject [description]
	 */
	public function setSubject($subject = '') {
		$this->subject = $subject;
	}

	/**
	 * [setHeaders description]
	 * @param array $headers [description]
	 */
	public function setHeaders($headers = array()) {
		$this->headers = $headers;
	}

	/**
	 * [getRecipients description]
	 * @return [type] [description]
	 */
	public function getRecipients() {
		return $this->recipients;
	}
}