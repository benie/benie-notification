<?php
namespace Benie\Notification;

abstract class BaseNotification {
	var $sender = '';
	var $recipients = array();
	var $body = '';

	abstract public function setRecipients($recipient);
	abstract public function getRecipients();

	/**
	 * [addRecipient description]
	 * @param [type] $recipient [description]
	 */
	final public function addRecipient($recipient) {
		$this->recipients[] = $recipient;
	}

	/**
	 * [setSender description]
	 * @param [type] $sender [description]
	 */
	public function setSender($sender) {
		$this->sender = $sender;
	}

	/**
	 * [setBody description]
	 * @param string $body [description]
	 */
	public function setBody($body = '') {
		$this->body = $body;
	}

	public function send() {
		if(empty($this->recipients))
			throw new \Exception('No recipients set for notification');

		return $this->dispatch();
	}
}