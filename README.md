# Benie Notifications

The **Benie Notifications** class allows developers to send Email and SMS
notifications to users.

## Services Supported
### Email
* [Amazon SES][ses] via the [AWS SDK for PHP][aws-github]
### SMS
* [SMS Broadcast][sms-broadcast] (Default)
* [Aussie SMS][aussie-sms]

## Installation
### composer.json
```json
{
  ...

  "repositories":
  [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:psddev/benie-notification.git"
    }
  ],

  ...

  "require": {
    "benie/notification": "dev-master"
  }
}
```

#### PHP 5.3
If running on a server with PHP 5.3, request the "php53" branch
```json
  "require": {
    "benie/notification": "dev-php53 as dev-master"
  }
```


The following environment variables must be set:
### Email
####SES
* `BenieEmailUser`: IAM Access KEY
* `BenieEmailPass`: IAM Secret KEY
* `BenieEmailFrom`: Email address messages will be sent from
*If these values are not present, the script will fall back to `EmailUser`, `EmailPass` and `EmailFrom`*

### SMS
####All Providers
* `BenieSMSUser`: Key/Username for SMS gateway
* `BenieSMSPass`: Password for SMS gateway
* `BenieSMSFrom`: Sender name
*If these values are not present, the script will fall back to `SMSUser`, `SMSPass` and `SMSFrom`*

## Usage
```php
<?php
require 'vendor/autoload.php';

use \Benie\notification;

$email = Notification::sendEmail([
  'recipients' => 'user@example.com',
  'sender' => 'sender@example.com',
  'body' => 'Hello World',
]);

$sms = Notification::sendSMS([
  'recipients' => '0400000000',
  'body' => 'Hello World!',
]);
```  

## Extending
### New Services
To send notifications from a new service, create a new trait in the
`src/Benie/Notification/[type]` folder implementing the `dispatch()` method.

For example

```php
<?php
// src/Benie/Notification/Email/xService.php

namespace Benie\Notification\Email;

trait xService {

  public function dispatch() {
    /**
     * Available properties:
     *
     * $this->recipients
     * $this->body
     * ... anything extra provided in the main service
     */

    return true;
  }
}
```

### Creating Notification Types
See `src/Benie/Notification/BaseNotification.php`

## Todo
* Implement Toast notifications
* Better documentation


[aws-github]: https://github.com/aws/aws-sdk-php
[ses]: http://aws.amazon.com/ses/

[sms-broadcast]: https://www.smsbroadcast.com.au
[aussie-sms]: https://www.aussiesms.com.au